console.log("Hello");

let trainer ={};

trainer.name = 'Ash';
trainer.age = 38;
trainer.pokemon = ['Picachu', 'Pidgeot', 'Kingler', 'Charizard', 'Bulbasaur', 'Primeape'];
trainer.friends = {
	misty: ['Kanto', 'Muk'],
	brock: ['Taurus', 'Lapras']

}

function Pokemon(name, level) {
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = 2 * level;

	this.tackle = function(target) {
		console.log(`${this.name} tackeled ${target.name}`)
		target.health -= this.attack;
		console.log(`${target.name}'s health is now reduced to ${target.health}`)
		if(target.health<= 0) {
			target.faint()
		}
	}

	this.faint = function() {
		console.log(this.name + 'fainted.')
	}

}

let pikachu = new Pokemon('Picachu', 100);
let pidgeot = new Pokemon('Pidgeout', 20);
let kingler = new Pokemon('Kingler', 50);


pikachu.tackle(pidgeot);
pidgeot.tackle(kingler);









